"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AgendaUserSchema extends Schema {
  up() {
    this.create("agenda_users", (table) => {
      table.increments();
      table.integer("user_id").unsigned().notNullable();
      table.integer("agenda_id").unsigned().notNullable();
      table.integer("role_agenda_id").unsigned().notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("agenda_users");
  }
}

module.exports = AgendaUserSchema;
