"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class EventSchema extends Schema {
  up() {
    this.create("events", (table) => {
      table.increments();
      table.integer("user_id").unsigned().notNullable();
      table.integer("agenda_id").unsigned().notNullable();
      table.date("eveDate").notNullable();
      table.time("eveStartTime").notNullable();
      table.time("eveEndTime").notNullable();
      table.string("eveName", 255).notNullable();
      table.string("eveImgName", 255);
      table.text("eveDescription");
      table.timestamps();
    });
  }

  down() {
    this.drop("events");
  }
}

module.exports = EventSchema;
