"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class ImageSchema extends Schema {
  up() {
    this.create("images", (table) => {
      table.increments();
      table.timestamps();
      table.binary("imaImage");
    });
  }

  down() {
    this.drop("images");
  }
}

module.exports = ImageSchema;
