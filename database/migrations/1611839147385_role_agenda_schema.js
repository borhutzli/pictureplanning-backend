"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class RoleAgendaSchema extends Schema {
  up() {
    this.create("role_agenda", (table) => {
      table.increments();
      table.string("rolName", 255).notNullable();
      table.text("rolDescription");
      table.timestamps();
    });
  }

  down() {
    this.drop("role_agenda");
  }
}

module.exports = RoleAgendaSchema;
