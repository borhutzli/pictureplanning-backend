"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AgendaSchema extends Schema {
  up() {
    this.create("agenda", (table) => {
      table.increments();
      table.string("ageName", 255).notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("agenda");
  }
}

module.exports = AgendaSchema;
