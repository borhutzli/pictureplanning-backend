"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.get("/", () => {
  return { greeting: "Hello world in JSON" };
});

Route.post("/signup", "UserController.signup");
Route.post("/login", "UserController.login");
Route.post("/new-event", "EventController.new").middleware(["auth:jwt"]);
Route.get("/me", "UserController.me").middleware(["auth:jwt"]);
Route.get("/agendas", "UserController.agendas").middleware(["auth:jwt"]);
Route.get("/events/:agenda/event/:id", "EventController.show").middleware([
  "auth:jwt",
]);
Route.get(
  "/events/:agenda/date/:date",
  "EventController.dateEvents"
).middleware(["auth:jwt"]);
Route.delete("events/delete/:id", "EventController.delete").middleware([
  "auth:jwt",
]);

Route.get("/test/:agenda/:date", "EventController.test");
