"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Agenda extends Model {
  users() {
    return this.belongsToMany(
      "App/Models/User",
      "agenda_id",
      "user_id"
    ).pivotTable("agenda_users");
  }

  events() {
    return this.hasMany("App/Models/Event");
  }
}

module.exports = Agenda;
