"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Event extends Model {
  user() {
    return this.belongsTo("App/Models/User");
  }

  agenda() {
    return this.belongsTo("App/Models/Agenda");
  }

  image() {
    return this.belongsTo("App/Models/Image");
  }

  static get dates() {
    return super.dates.concat(["eveDate"]);
  }
}

module.exports = Event;
