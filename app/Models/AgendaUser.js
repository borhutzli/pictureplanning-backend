"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class AgendaUser extends Model {
  role() {
    return this.belongsTo("App/Models/RoleAgendum");
  }
}

module.exports = AgendaUser;
