"use strict";
const User = use("App/Models/User");
const Agenda = use("App/Models/Agenda");
const AgendaUser = use("App/Models/AgendaUser");
const Database = use("Database");

class UserController {
  async me({ auth, response }) {
    const user = await User.query()
      .where("id", auth.current.user.id)
      .firstOrFail();

    console.log(user);
    return response.json({
      status: "success",
      data: user,
    });
  }

  async signup({ request, auth, response }) {
    const userData = request.only([
      "useName",
      "useSurname",
      "useUsername",
      "email",
      "password",
    ]);

    try {
      const user = await User.create(userData);
      const agenda = await Agenda.create({ ageName: "New agenda" });
      const agendaUser = await AgendaUser.create({
        user_id: user.id,
        agenda_id: agenda.id,
        role_agenda_id: 1,
      });
      const token = await auth.generate(user);

      return response.json({
        status: "success",
        data: token,
      });
    } catch (error) {
      console.log(error);
      return response.status(400).json({
        status: "error",
        message:
          "there was a problem creating the user, please try again later.",
      });
    }
  }

  async login({ request, auth, response }) {
    try {
      const token = await auth.attempt(
        request.input("email"),
        request.input("password")
      );

      return response.json({
        status: "success",
        data: token,
      });
    } catch (error) {
      console.log(error);
      response.status(400).json({
        status: "error",
        message: "Invalid email/password",
      });
    }
  }

  async agendas({ auth, response }) {
    try {
      const user = await User.find(auth.current.user.id);
      const agendas = await user.agendas().fetch();

      const agendasJSON = agendas.toJSON();
      const agendasOwners = [];
      for (let i = 0; i < agendasJSON.length; i++) {
        const agendaOwnerId = await Database.select("user_id")
          .from("agenda_users")
          .where({ role_agenda_id: 1, agenda_id: agendasJSON[i].id });
        const ownerUser = await User.find(agendaOwnerId[0].user_id);
        agendasOwners.push(ownerUser);
      }

      return response.json({
        status: "success",
        agendas: agendas,
        owners: agendasOwners,
      });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = UserController;
