"use strict";
const Event = use("App/Models/Event");
const Database = use("Database");

class EventController {
  async new({ request, auth, response }) {
    const userAgendaRole = await Database.select("role_agenda_id")
      .from("agenda_users")
      .where({
        user_id: auth.current.user.id,
        agenda_id: request.input("agenda_id"),
      });

    if (userAgendaRole[0].role_agenda_id != 1) {
      return response.status(400).json({
        status: "error",
        message: "Access denied.",
      });
    }

    try {
      const event = new Event();
      event.user_id = auth.current.user.id;
      event.agenda_id = request.input("agenda_id");
      event.eveImgName = request.input("eveImgName");
      event.eveDate = request.input("eveDate");
      event.eveStartTime = request.input("eveStartTime");
      event.eveEndTime = request.input("eveEndTime");
      event.eveName = request.input("eveName");
      event.eveDescription = request.input("eveDescription");
      await event.save();

      return response.json({
        status: "success",
        data: event,
      });
    } catch (error) {
      console.log(error);
      return response.status(400).json({
        status: "error",
        message:
          "there was a problem creating the user, please try again later.",
      });
    }
  }

  async show({ params, auth, response }) {
    const userAgendaRole = await Database.select("role_agenda_id")
      .from("agenda_users")
      .where({
        user_id: auth.current.user.id,
        agenda_id: params.agenda,
      });

    if (!!!userAgendaRole[0].role_agenda_id) {
      return response.status(400).json({
        status: "error",
        message: "Access denied.",
      });
    }
    try {
      const event = await Event.query().where("id", params.id).firstOrFail();
      return response.json({
        status: "success",
        data: event,
      });
    } catch (error) {
      return response.status(404).json({
        status: "error",
        message: "Event not found",
      });
    }
  }

  async dateEvents({ params, auth, response }) {
    try {
      const date = params.date;
      const agendaId = params.agenda;
      const userAgendaRole = await Database.select("role_agenda_id")
        .from("agenda_users")
        .where({
          user_id: auth.current.user.id,
          agenda_id: agendaId,
        });

      if (!!!userAgendaRole[0].role_agenda_id) {
        return response.status(400).json({
          status: "error",
          message: "Access denied.",
        });
      }

      const events = await Event.query()
        .where({ eveDate: date, agenda_id: agendaId })
        .fetch();

      return response.json({
        status: "success",
        data: events,
      });
    } catch (error) {
      console.log(error);
    }
  }

  async delete({ request, auth, params, response }) {
    const user = auth.current.user;

    const event = await Event.query()
      .where("user_id", user.id)
      .where("id", params.id)
      .firstOrFail();

    await event.delete();

    return response.json({
      status: "success",
      message: "Event deleted",
    });
  }
}

module.exports = EventController;
